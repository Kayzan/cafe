﻿namespace Cafe
{
    partial class FrmCafe
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.GrpAppetizers = new System.Windows.Forms.GroupBox();
            this.RbtSoup = new System.Windows.Forms.RadioButton();
            this.RbtSalad = new System.Windows.Forms.RadioButton();
            this.GrpMains = new System.Windows.Forms.GroupBox();
            this.RbtChicken = new System.Windows.Forms.RadioButton();
            this.RbtLasagna = new System.Windows.Forms.RadioButton();
            this.RbtSteak = new System.Windows.Forms.RadioButton();
            this.GrpRefreshments = new System.Windows.Forms.GroupBox();
            this.RbtPop = new System.Windows.Forms.RadioButton();
            this.RbtWater = new System.Windows.Forms.RadioButton();
            this.RbtJuice = new System.Windows.Forms.RadioButton();
            this.TxtAppetizer = new System.Windows.Forms.TextBox();
            this.LblAppetizer = new System.Windows.Forms.Label();
            this.TxtMain = new System.Windows.Forms.TextBox();
            this.LblMain = new System.Windows.Forms.Label();
            this.TxtRefreshments = new System.Windows.Forms.TextBox();
            this.LblRefreshments = new System.Windows.Forms.Label();
            this.TxtSubtotal = new System.Windows.Forms.TextBox();
            this.LblSubtotal = new System.Windows.Forms.Label();
            this.TxtTax = new System.Windows.Forms.TextBox();
            this.LblTax = new System.Windows.Forms.Label();
            this.TxtTotal = new System.Windows.Forms.TextBox();
            this.LblTotal = new System.Windows.Forms.Label();
            this.BtnCalculate = new System.Windows.Forms.Button();
            this.BtnNewOrder = new System.Windows.Forms.Button();
            this.BtnExit = new System.Windows.Forms.Button();
            this.GrpAppetizers.SuspendLayout();
            this.GrpMains.SuspendLayout();
            this.GrpRefreshments.SuspendLayout();
            this.SuspendLayout();
            // 
            // GrpAppetizers
            // 
            this.GrpAppetizers.Controls.Add(this.RbtSalad);
            this.GrpAppetizers.Controls.Add(this.RbtSoup);
            this.GrpAppetizers.Location = new System.Drawing.Point(283, 43);
            this.GrpAppetizers.Name = "GrpAppetizers";
            this.GrpAppetizers.Size = new System.Drawing.Size(145, 140);
            this.GrpAppetizers.TabIndex = 0;
            this.GrpAppetizers.TabStop = false;
            this.GrpAppetizers.Text = "Appetizers";
            this.GrpAppetizers.Enter += new System.EventHandler(this.GrpAppetizers_Enter);
            // 
            // RbtSoup
            // 
            this.RbtSoup.AutoSize = true;
            this.RbtSoup.Location = new System.Drawing.Point(17, 38);
            this.RbtSoup.Name = "RbtSoup";
            this.RbtSoup.Size = new System.Drawing.Size(80, 17);
            this.RbtSoup.TabIndex = 0;
            this.RbtSoup.TabStop = true;
            this.RbtSoup.Text = "Soup $2.00";
            this.RbtSoup.UseVisualStyleBackColor = true;
            this.RbtSoup.CheckedChanged += new System.EventHandler(this.RbtSoup_CheckedChanged);
            // 
            // RbtSalad
            // 
            this.RbtSalad.AutoSize = true;
            this.RbtSalad.Location = new System.Drawing.Point(17, 90);
            this.RbtSalad.Name = "RbtSalad";
            this.RbtSalad.Size = new System.Drawing.Size(82, 17);
            this.RbtSalad.TabIndex = 1;
            this.RbtSalad.TabStop = true;
            this.RbtSalad.Text = "Salad $2.50";
            this.RbtSalad.UseVisualStyleBackColor = true;
            this.RbtSalad.CheckedChanged += new System.EventHandler(this.RbtSalad_CheckedChanged);
            // 
            // GrpMains
            // 
            this.GrpMains.Controls.Add(this.RbtSteak);
            this.GrpMains.Controls.Add(this.RbtLasagna);
            this.GrpMains.Controls.Add(this.RbtChicken);
            this.GrpMains.Location = new System.Drawing.Point(495, 51);
            this.GrpMains.Name = "GrpMains";
            this.GrpMains.Size = new System.Drawing.Size(147, 166);
            this.GrpMains.TabIndex = 1;
            this.GrpMains.TabStop = false;
            this.GrpMains.Text = "Mains";
            this.GrpMains.Enter += new System.EventHandler(this.GrpMains_Enter);
            // 
            // RbtChicken
            // 
            this.RbtChicken.AutoSize = true;
            this.RbtChicken.Location = new System.Drawing.Point(14, 32);
            this.RbtChicken.Name = "RbtChicken";
            this.RbtChicken.Size = new System.Drawing.Size(100, 17);
            this.RbtChicken.TabIndex = 0;
            this.RbtChicken.TabStop = true;
            this.RbtChicken.Text = "Chicken $10.50";
            this.RbtChicken.UseVisualStyleBackColor = true;
            this.RbtChicken.CheckedChanged += new System.EventHandler(this.RbtChicken_CheckedChanged);
            // 
            // RbtLasagna
            // 
            this.RbtLasagna.AutoSize = true;
            this.RbtLasagna.Location = new System.Drawing.Point(14, 82);
            this.RbtLasagna.Name = "RbtLasagna";
            this.RbtLasagna.Size = new System.Drawing.Size(102, 17);
            this.RbtLasagna.TabIndex = 1;
            this.RbtLasagna.TabStop = true;
            this.RbtLasagna.Text = "Lasagna $11.75";
            this.RbtLasagna.UseVisualStyleBackColor = true;
            this.RbtLasagna.CheckedChanged += new System.EventHandler(this.RbtLasagna_CheckedChanged);
            // 
            // RbtSteak
            // 
            this.RbtSteak.AutoSize = true;
            this.RbtSteak.Location = new System.Drawing.Point(14, 124);
            this.RbtSteak.Name = "RbtSteak";
            this.RbtSteak.Size = new System.Drawing.Size(89, 17);
            this.RbtSteak.TabIndex = 2;
            this.RbtSteak.TabStop = true;
            this.RbtSteak.Text = "Steak $15.00";
            this.RbtSteak.UseVisualStyleBackColor = true;
            this.RbtSteak.CheckedChanged += new System.EventHandler(this.RbtSteak_CheckedChanged);
            // 
            // GrpRefreshments
            // 
            this.GrpRefreshments.Controls.Add(this.RbtJuice);
            this.GrpRefreshments.Controls.Add(this.RbtWater);
            this.GrpRefreshments.Controls.Add(this.RbtPop);
            this.GrpRefreshments.Location = new System.Drawing.Point(283, 211);
            this.GrpRefreshments.Name = "GrpRefreshments";
            this.GrpRefreshments.Size = new System.Drawing.Size(145, 143);
            this.GrpRefreshments.TabIndex = 2;
            this.GrpRefreshments.TabStop = false;
            this.GrpRefreshments.Text = "Refreshments";
            this.GrpRefreshments.Enter += new System.EventHandler(this.GrpRefreshments_Enter);
            // 
            // RbtPop
            // 
            this.RbtPop.AutoSize = true;
            this.RbtPop.Location = new System.Drawing.Point(15, 30);
            this.RbtPop.Name = "RbtPop";
            this.RbtPop.Size = new System.Drawing.Size(74, 17);
            this.RbtPop.TabIndex = 0;
            this.RbtPop.TabStop = true;
            this.RbtPop.Text = "Pop $1.25";
            this.RbtPop.UseVisualStyleBackColor = true;
            this.RbtPop.CheckedChanged += new System.EventHandler(this.RbtPop_CheckedChanged);
            // 
            // RbtWater
            // 
            this.RbtWater.AutoSize = true;
            this.RbtWater.Location = new System.Drawing.Point(15, 67);
            this.RbtWater.Name = "RbtWater";
            this.RbtWater.Size = new System.Drawing.Size(84, 17);
            this.RbtWater.TabIndex = 1;
            this.RbtWater.TabStop = true;
            this.RbtWater.Text = "Water $1.00";
            this.RbtWater.UseVisualStyleBackColor = true;
            this.RbtWater.CheckedChanged += new System.EventHandler(this.RbtWater_CheckedChanged);
            // 
            // RbtJuice
            // 
            this.RbtJuice.AutoSize = true;
            this.RbtJuice.Location = new System.Drawing.Point(15, 102);
            this.RbtJuice.Name = "RbtJuice";
            this.RbtJuice.Size = new System.Drawing.Size(80, 17);
            this.RbtJuice.TabIndex = 2;
            this.RbtJuice.TabStop = true;
            this.RbtJuice.Text = "Juice $1.30";
            this.RbtJuice.UseVisualStyleBackColor = true;
            this.RbtJuice.CheckedChanged += new System.EventHandler(this.RbtJuice_CheckedChanged);
            // 
            // TxtAppetizer
            // 
            this.TxtAppetizer.Location = new System.Drawing.Point(135, 56);
            this.TxtAppetizer.Name = "TxtAppetizer";
            this.TxtAppetizer.Size = new System.Drawing.Size(103, 20);
            this.TxtAppetizer.TabIndex = 3;
            this.TxtAppetizer.TextChanged += new System.EventHandler(this.TxtAppetizer_TextChanged);
            // 
            // LblAppetizer
            // 
            this.LblAppetizer.AutoSize = true;
            this.LblAppetizer.Location = new System.Drawing.Point(12, 56);
            this.LblAppetizer.Name = "LblAppetizer";
            this.LblAppetizer.Size = new System.Drawing.Size(51, 13);
            this.LblAppetizer.TabIndex = 4;
            this.LblAppetizer.Text = "Appetizer";
            this.LblAppetizer.Click += new System.EventHandler(this.LblAppetizer_Click);
            // 
            // TxtMain
            // 
            this.TxtMain.Location = new System.Drawing.Point(135, 98);
            this.TxtMain.Name = "TxtMain";
            this.TxtMain.Size = new System.Drawing.Size(103, 20);
            this.TxtMain.TabIndex = 5;
            this.TxtMain.TextChanged += new System.EventHandler(this.TxtMain_TextChanged);
            // 
            // LblMain
            // 
            this.LblMain.AutoSize = true;
            this.LblMain.Location = new System.Drawing.Point(13, 98);
            this.LblMain.Name = "LblMain";
            this.LblMain.Size = new System.Drawing.Size(30, 13);
            this.LblMain.TabIndex = 6;
            this.LblMain.Text = "Main";
            this.LblMain.Click += new System.EventHandler(this.LblMain_Click);
            // 
            // TxtRefreshments
            // 
            this.TxtRefreshments.Location = new System.Drawing.Point(135, 146);
            this.TxtRefreshments.Name = "TxtRefreshments";
            this.TxtRefreshments.Size = new System.Drawing.Size(103, 20);
            this.TxtRefreshments.TabIndex = 7;
            this.TxtRefreshments.TextChanged += new System.EventHandler(this.TxtRefreshments_TextChanged);
            // 
            // LblRefreshments
            // 
            this.LblRefreshments.AutoSize = true;
            this.LblRefreshments.Location = new System.Drawing.Point(12, 146);
            this.LblRefreshments.Name = "LblRefreshments";
            this.LblRefreshments.Size = new System.Drawing.Size(72, 13);
            this.LblRefreshments.TabIndex = 8;
            this.LblRefreshments.Text = "Refreshments";
            this.LblRefreshments.Click += new System.EventHandler(this.LblRefreshments_Click);
            // 
            // TxtSubtotal
            // 
            this.TxtSubtotal.Location = new System.Drawing.Point(135, 245);
            this.TxtSubtotal.Name = "TxtSubtotal";
            this.TxtSubtotal.Size = new System.Drawing.Size(103, 20);
            this.TxtSubtotal.TabIndex = 9;
            this.TxtSubtotal.TextChanged += new System.EventHandler(this.TxtSubtotal_TextChanged);
            // 
            // LblSubtotal
            // 
            this.LblSubtotal.AutoSize = true;
            this.LblSubtotal.Location = new System.Drawing.Point(12, 245);
            this.LblSubtotal.Name = "LblSubtotal";
            this.LblSubtotal.Size = new System.Drawing.Size(46, 13);
            this.LblSubtotal.TabIndex = 10;
            this.LblSubtotal.Text = "Subtotal";
            this.LblSubtotal.Click += new System.EventHandler(this.LblSubtotal_Click);
            // 
            // TxtTax
            // 
            this.TxtTax.Location = new System.Drawing.Point(135, 292);
            this.TxtTax.Name = "TxtTax";
            this.TxtTax.Size = new System.Drawing.Size(103, 20);
            this.TxtTax.TabIndex = 11;
            this.TxtTax.TextChanged += new System.EventHandler(this.TxtTax_TextChanged);
            // 
            // LblTax
            // 
            this.LblTax.AutoSize = true;
            this.LblTax.Location = new System.Drawing.Point(13, 295);
            this.LblTax.Name = "LblTax";
            this.LblTax.Size = new System.Drawing.Size(54, 13);
            this.LblTax.TabIndex = 12;
            this.LblTax.Text = "Tax (13%)";
            this.LblTax.Click += new System.EventHandler(this.LblTax_Click);
            // 
            // TxtTotal
            // 
            this.TxtTotal.Location = new System.Drawing.Point(135, 349);
            this.TxtTotal.Name = "TxtTotal";
            this.TxtTotal.Size = new System.Drawing.Size(103, 20);
            this.TxtTotal.TabIndex = 13;
            this.TxtTotal.TextChanged += new System.EventHandler(this.TxtTotal_TextChanged);
            // 
            // LblTotal
            // 
            this.LblTotal.AutoSize = true;
            this.LblTotal.Location = new System.Drawing.Point(13, 352);
            this.LblTotal.Name = "LblTotal";
            this.LblTotal.Size = new System.Drawing.Size(31, 13);
            this.LblTotal.TabIndex = 14;
            this.LblTotal.Text = "Total";
            this.LblTotal.Click += new System.EventHandler(this.LblTotal_Click);
            // 
            // BtnCalculate
            // 
            this.BtnCalculate.Location = new System.Drawing.Point(338, 393);
            this.BtnCalculate.Name = "BtnCalculate";
            this.BtnCalculate.Size = new System.Drawing.Size(118, 31);
            this.BtnCalculate.TabIndex = 15;
            this.BtnCalculate.Text = "Calculate Bill";
            this.BtnCalculate.UseVisualStyleBackColor = true;
            this.BtnCalculate.Click += new System.EventHandler(this.BtnCalculate_Click);
            // 
            // BtnNewOrder
            // 
            this.BtnNewOrder.Location = new System.Drawing.Point(492, 393);
            this.BtnNewOrder.Name = "BtnNewOrder";
            this.BtnNewOrder.Size = new System.Drawing.Size(116, 30);
            this.BtnNewOrder.TabIndex = 16;
            this.BtnNewOrder.Text = "New Order";
            this.BtnNewOrder.UseVisualStyleBackColor = true;
            this.BtnNewOrder.Click += new System.EventHandler(this.BtnNewOrder_Click);
            // 
            // BtnExit
            // 
            this.BtnExit.Location = new System.Drawing.Point(640, 395);
            this.BtnExit.Name = "BtnExit";
            this.BtnExit.Size = new System.Drawing.Size(115, 28);
            this.BtnExit.TabIndex = 17;
            this.BtnExit.Text = "Exit";
            this.BtnExit.UseVisualStyleBackColor = true;
            this.BtnExit.Click += new System.EventHandler(this.BtnExit_Click);
            // 
            // FrmCafe
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(800, 450);
            this.Controls.Add(this.BtnExit);
            this.Controls.Add(this.BtnNewOrder);
            this.Controls.Add(this.BtnCalculate);
            this.Controls.Add(this.LblTotal);
            this.Controls.Add(this.TxtTotal);
            this.Controls.Add(this.LblTax);
            this.Controls.Add(this.TxtTax);
            this.Controls.Add(this.LblSubtotal);
            this.Controls.Add(this.TxtSubtotal);
            this.Controls.Add(this.LblRefreshments);
            this.Controls.Add(this.TxtRefreshments);
            this.Controls.Add(this.LblMain);
            this.Controls.Add(this.TxtMain);
            this.Controls.Add(this.LblAppetizer);
            this.Controls.Add(this.TxtAppetizer);
            this.Controls.Add(this.GrpRefreshments);
            this.Controls.Add(this.GrpMains);
            this.Controls.Add(this.GrpAppetizers);
            this.Name = "FrmCafe";
            this.Text = "Cafe";
            this.Load += new System.EventHandler(this.FrmCafe_Load);
            this.GrpAppetizers.ResumeLayout(false);
            this.GrpAppetizers.PerformLayout();
            this.GrpMains.ResumeLayout(false);
            this.GrpMains.PerformLayout();
            this.GrpRefreshments.ResumeLayout(false);
            this.GrpRefreshments.PerformLayout();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.GroupBox GrpAppetizers;
        private System.Windows.Forms.RadioButton RbtSalad;
        private System.Windows.Forms.RadioButton RbtSoup;
        private System.Windows.Forms.GroupBox GrpMains;
        private System.Windows.Forms.RadioButton RbtSteak;
        private System.Windows.Forms.RadioButton RbtLasagna;
        private System.Windows.Forms.RadioButton RbtChicken;
        private System.Windows.Forms.GroupBox GrpRefreshments;
        private System.Windows.Forms.RadioButton RbtWater;
        private System.Windows.Forms.RadioButton RbtPop;
        private System.Windows.Forms.RadioButton RbtJuice;
        private System.Windows.Forms.TextBox TxtAppetizer;
        private System.Windows.Forms.Label LblAppetizer;
        private System.Windows.Forms.TextBox TxtMain;
        private System.Windows.Forms.Label LblMain;
        private System.Windows.Forms.TextBox TxtRefreshments;
        private System.Windows.Forms.Label LblRefreshments;
        private System.Windows.Forms.TextBox TxtSubtotal;
        private System.Windows.Forms.Label LblSubtotal;
        private System.Windows.Forms.TextBox TxtTax;
        private System.Windows.Forms.Label LblTax;
        private System.Windows.Forms.TextBox TxtTotal;
        private System.Windows.Forms.Label LblTotal;
        private System.Windows.Forms.Button BtnCalculate;
        private System.Windows.Forms.Button BtnNewOrder;
        private System.Windows.Forms.Button BtnExit;
    }
}

