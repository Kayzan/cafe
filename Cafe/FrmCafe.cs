﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Cafe
{
    public partial class FrmCafe : Form
    {
        public FrmCafe()
        {
            InitializeComponent();
        }

        double soup, salad;
        double chicken, lasagna, steak;
        double pop, water, juice;
        double subtotal, tax, total;
        double appetizer, main, refreshment;

        

        private void FrmCafe_Load(object sender, EventArgs e)
        {
            soup = 2.00;
            salad = 2.50;
            chicken = 10.50;
            lasagna = 11.75;
            steak = 15.00;
            pop = 1.25;
            water = 1.00;
            juice = 1.30;

            appetizer = 0;
            main = 0;
            refreshment = 0;

            

        }

        private void GrpAppetizers_Enter(object sender, EventArgs e)
        {

        }

        private void RbtSoup_CheckedChanged(object sender, EventArgs e)
        {
            appetizer = soup;
            subtotal = appetizer + main + refreshment;
            tax = subtotal * 0.13;

            subtotal = appetizer + main + refreshment;
            TxtAppetizer.Text = appetizer.ToString("c");
            TxtSubtotal.Text = subtotal.ToString("c");

        }

        private void RbtSalad_CheckedChanged(object sender, EventArgs e)
        {
            appetizer = salad;
            subtotal = appetizer + main + refreshment;
            TxtAppetizer.Text = appetizer.ToString("c");
            TxtSubtotal.Text = subtotal.ToString("c");
        }

        private void GrpMains_Enter(object sender, EventArgs e)
        {
            
        }

        private void RbtChicken_CheckedChanged(object sender, EventArgs e)
        {
            main = chicken;
            subtotal = appetizer + main + refreshment;
            TxtMain.Text = main.ToString("c");
            TxtSubtotal.Text = subtotal.ToString("c");




        }

        private void RbtLasagna_CheckedChanged(object sender, EventArgs e)
        {
            main = lasagna;
            subtotal = appetizer + main + refreshment;
            TxtMain.Text = main.ToString("c");
            TxtSubtotal.Text = subtotal.ToString("c");
        }

        private void RbtSteak_CheckedChanged(object sender, EventArgs e)
        {
            main = steak;
            subtotal = appetizer + main + refreshment;
            TxtMain.Text = main.ToString("c");
            TxtSubtotal.Text = subtotal.ToString("c");
        }

        private void GrpRefreshments_Enter(object sender, EventArgs e)
        {

        }

        private void RbtWater_CheckedChanged(object sender, EventArgs e)
        {
            refreshment = water;
            subtotal = appetizer + main + refreshment;
            TxtRefreshments.Text = refreshment.ToString("c");
            TxtSubtotal.Text = subtotal.ToString("c");
        }

        private void RbtPop_CheckedChanged(object sender, EventArgs e)
        {
            refreshment = pop;
            subtotal = appetizer + main + refreshment;
            TxtRefreshments.Text = refreshment.ToString("c");
            TxtSubtotal.Text = subtotal.ToString("c");
        }

        private void RbtJuice_CheckedChanged(object sender, EventArgs e)
        {
            refreshment = juice;
            subtotal = appetizer + main + refreshment;
            TxtRefreshments.Text = refreshment.ToString("c");
            TxtSubtotal.Text = subtotal.ToString("c");
        }

        private void TxtAppetizer_TextChanged(object sender, EventArgs e)
        {

        }

        private void LblAppetizer_Click(object sender, EventArgs e)
        {

        }

        private void TxtMain_TextChanged(object sender, EventArgs e)
        {

        }

        private void LblMain_Click(object sender, EventArgs e)
        {

        }

        private void TxtRefreshments_TextChanged(object sender, EventArgs e)
        {

        }

        private void LblRefreshments_Click(object sender, EventArgs e)
        {

        }

        private void TxtSubtotal_TextChanged(object sender, EventArgs e)
        {

        }

        private void LblSubtotal_Click(object sender, EventArgs e)
        {

        }

        private void TxtTax_TextChanged(object sender, EventArgs e)
        {

        }

        private void LblTax_Click(object sender, EventArgs e)
        {

        }

        private void TxtTotal_TextChanged(object sender, EventArgs e)
        {

        }

        private void LblTotal_Click(object sender, EventArgs e)
        {

        }

        private void BtnCalculate_Click(object sender, EventArgs e)
        {
            
            

            tax = subtotal * 0.14;
            TxtTax.Text = tax.ToString("c");

            total = subtotal + tax;
            TxtTotal.Text = total.ToString("c");
        }

        private void BtnNewOrder_Click(object sender, EventArgs e)
        {
            RbtSoup.Checked = false;
            RbtSalad.Checked = false;
            RbtChicken.Checked = false;
            RbtLasagna.Checked = false;
            RbtSteak.Checked = false;
            RbtPop.Checked = false;
            RbtWater.Checked = false;
            RbtJuice.Checked = false;

            TxtAppetizer.Text = "";
            TxtMain.Text = "";
            TxtRefreshments.Text = "";

            appetizer = 0;
            main = 0;
            refreshment = 0;
            TxtSubtotal.Text = "";
            TxtTax.Text = "";
            TxtTotal.Text = "";
            
        }

        private void BtnExit_Click(object sender, EventArgs e)
        {
            this.Close();
        }
    }
}
